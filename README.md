Requirements:
- Java 8.
- Maven 3.3.x.
- Tomcat (any recent version will do).
- IntelliJ 14 or greater.
- MySQL server.

Setup:

1. Edit the src/main/java/org/metropolis/blogger/configuration/EventDataConfiguration.java and update the DriverManagerDataSource
   method, specifically the driverManagerDataSource.setUsername/setPassword/setUrl entries - change the username, password and
   url values to the ones specific for your database.

2. Run the src/main/sql/contacts.ddl SQL script in your configured database (this will create the data structure).

3. Under intelliJ, edit the configuration and add your Tomcat server - make sure you also specify an artifact to deploy as well.

4. run 'mvn clean' and 'mvn compile.'

5. Now, start tomcat and point your browser to http://localhost:8080

6. Note: if you'd like to run the tests, make sure you edit the EventDataConfigurationTest.java file and update the DriverManagerDataSource
   driverManagerDataSource values (password, username, etc) just like you did in step one.