package org.metropolis.events.controller;

import org.metropolis.events.entity.EventUser;
import org.metropolis.events.service.AuthService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by cgrieb on 11/30/16.
 */
@Controller()
@RequestMapping(value = "/auth")
public class AuthController {
    @Resource
    AuthService authService;

    /**
     * Extracts users from the @AuthenticationPrincipal injection.
     * @param userDetails - UserDetails.
     * @return userDetails object.
     */
    @PreAuthorize("hasAnyRole('admin','user')")
    @RequestMapping(value = "/getUserDetails", method = RequestMethod.POST)
    public @ResponseBody UserDetails getUserDetails(@AuthenticationPrincipal UserDetails userDetails) {
       return userDetails;
    }

    /**
     * Get authentication status for the current session.
     * @return session has an authenticated principle/session does not have an authenticated principle.
     */
    @RequestMapping(value = "/getAuthStatus", method = RequestMethod.GET)
    public @ResponseBody boolean getAuthStatus() {
        return authService.getAuthStatus();
    }

    /**
     * Adds a new eventUser object to the event_user table - this user will be able to login with
     * authorization 'user.'
     * @param eventUser - eventUser object.
     * @return successfully created account/did not successfully create account.
     */
    @RequestMapping(value = "/createUserAccount", method = RequestMethod.POST)
    public @ResponseBody boolean createUserAccount(@RequestBody EventUser eventUser) {
        return authService.createAccount(eventUser);
    }

    /**
     * Logs a user securely into the event system.
     * @param httpServletRequest - HttpServletRequest with Auth header set to contain credentials.
     * @return logged in/did not login.
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public @ResponseBody boolean login(HttpServletRequest httpServletRequest) {
        return authService.login(httpServletRequest);
    }

    /**
     * Logs a user out of the event system.
     * @return logged out/did not logout.
     */
    @PreAuthorize("hasAnyRole('admin','user')")
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public @ResponseBody boolean logout() {
        return authService.logout();
    }
}
