package org.metropolis.events.controller;

import org.metropolis.events.service.CacheService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by metropolis on 1/2/2017.
 */
@Controller
@RequestMapping(value = "/admin")
@PreAuthorize("hasAnyRole('admin')")
public class CacheController {
    @Resource
    private CacheService cacheService;

    /**
     * Clear out system cache.
     * @return - date (in string format) that the system cache was cleared on.
     */
    @RequestMapping(value = "/clearAllSystemCache", method = RequestMethod.GET)
    public @ResponseBody
    Date clearAllSystemCache() {
        return cacheService.clearAllSystemCache();
    }
}
