package org.metropolis.events.controller;

import org.metropolis.events.entity.Event;
import org.metropolis.events.entity.Ticket;
import org.metropolis.events.service.EventService;
import org.metropolis.events.service.TicketService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by cgrieb on 12/11/16.
 */
@Controller
@RequestMapping(value = "/event")
public class EventController {
    @Resource
    private EventService eventService;

    @Resource
    private TicketService ticketService;

    /**
     * Entry point for saving an event.
     * @param event - Event object.
     */
    @PreAuthorize("hasAnyRole('admin')")
    @RequestMapping(value = "/addEvent", method = RequestMethod.POST)
    public @ResponseBody void addEvent(@RequestBody Event event) {
        eventService.addEvent(event);
    }

    /**
     * Entry point for getting all active, displayable events.
     * @return - A List of all active events.
     */
    @PreAuthorize("hasAnyRole('admin','user')")
    @RequestMapping(value = "/getActiveEvents", method = RequestMethod.GET)
    public @ResponseBody List<Event> getActiveEvents() {
        return eventService.getActiveEvents();
    }

    /**
     * Get all active events and the associated tickets with them.
     * @param userDetails - @AuthenticationPrinciple UserDetails.
     * @return A Map String,Object of events and tickets.
     */
    @PreAuthorize("hasAnyRole('admin','user')")
    @RequestMapping(value = "/getEventsAndTickets", method = RequestMethod.GET)
    public @ResponseBody Map<String,Object> getEventsAndTickets(@AuthenticationPrincipal UserDetails userDetails) {
        return eventService.getEventsAndTickets(userDetails);
    }

    /**
     * Entry point for purchasing tickets.
     * @param ticket - Tickets object containing tickets for an event.
     */
    @PreAuthorize("hasAnyRole('admin','user')")
    @RequestMapping(value = "/purchaseTicketsForEvent", method = RequestMethod.POST)
    public @ResponseBody boolean purchaseTicketsForEvent(@RequestBody Ticket ticket,  @AuthenticationPrincipal   UserDetails userDetails) {
        return ticketService.purchaseTicketsForEvent(ticket, userDetails.getUsername());
    }
}
