package org.metropolis.events.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by cgrieb on 11/30/16.
 */
@Entity
@Table(name = "events")
public class Event {
    @Id
    @Column(name = "EVENT_ID")
    int eventId;

    @Column(name = "AVAILABLE_TICKETS")
    int availableTickets;

    @Column(name = "DESCRIPTION")
    String description;

    @Column(name = "TITLE")
    String title;

    @Column(name = "START_DATE")
    Date startDate;

    @Column(name = "END_DATE")
    Date endDate;

    @Column(name = "ENABLED")
    boolean enabled;

    @Column(name = "TICKET_PRICE")
    int ticketPrice;

    @Column(name = "TICKET_PURCHASE_MAX")
    int ticketPurchaseMax;

    @Column(name = "CREATED_BY")
    private String createdBy;

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getAvailableTickets() {
        return availableTickets;
    }

    public void setAvailableTickets(int availableTickets) {
        this.availableTickets = availableTickets;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public int getTicketPurchaseMax() {
        return ticketPurchaseMax;
    }

    public void setTicketPurchaseMax(int ticketPurchaseMax) {
        this.ticketPurchaseMax = ticketPurchaseMax;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
