package org.metropolis.events.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by cgrieb on 11/30/16.
 */
@Table(name = "event_user_roles")
@Entity
public class EventUserRole {
    @Id
    @Column(name = "USERNAME")
    private String username;

    @Column(name = "ROLE")
    private String role;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
