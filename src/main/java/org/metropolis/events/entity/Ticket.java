package org.metropolis.events.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by cgrieb on 11/30/16.
 */
@Entity
@Table(name = "tickets")
public class Ticket {
    @Id
    @Column(name = "TICKET_ID")
    private int ticketId;

    @Column(name = "TICKET_OWNER")
    private String ticketOwner;

    @Column(name = "PURCHASED_TICKETS")
    private int purchasedTickets;

    @Column(name = "PURCHASE_DATE")
    private Date purchasedDate;

    @Column(name = "PAID_DATE")
    private Date paidDate;

    @Column(name = "PAID")
    private boolean paid;

    @Column(name = "EVENT_ID")
    private int eventId;

    public int getTicketId() {return ticketId;}

    public void setTicketId(int ticketId) {this.ticketId = ticketId;}

    public String getTicketOwner() {
        return ticketOwner;
    }

    public void setTicketOwner(String ticketOwner) {
        this.ticketOwner = ticketOwner;
    }

    public int getPurchasedTickets() {
        return purchasedTickets;
    }

    public void setPurchasedTickets(int purchasedTickets) {
        this.purchasedTickets = purchasedTickets;
    }

    public Date getPurchasedDate() {
        return purchasedDate;
    }

    public void setPurchasedDate(Date purchasedDate) {
        this.purchasedDate = purchasedDate;
    }

    public Date getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Date paidDate) {
        this.paidDate = paidDate;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public int getEventId() {return eventId;}

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }
}
