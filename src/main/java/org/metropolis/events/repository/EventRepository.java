package org.metropolis.events.repository;

import org.metropolis.events.entity.Event;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cgrieb on 12/11/16.
 */
@Repository
public interface EventRepository extends JpaRepository<Event,Integer> {
    @Cacheable(value = "eventCache")
    @Query("select e from Event e where e.enabled = true and e.endDate >= current_date order by e.startDate")
    List<Event> getAllActiveEvents();
}
