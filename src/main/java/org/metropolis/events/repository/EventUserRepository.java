package org.metropolis.events.repository;

import org.metropolis.events.entity.EventUser;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by cgrieb on 12/2/16.
 */
@Repository
public interface EventUserRepository extends JpaRepository<EventUser,String> {
    @Override
    @Query("select u from EventUser u where u.username =:username")
    @Cacheable(value = "userCache", key = "#username")
    EventUser findOne(@Param(value = "username") String username);
}
