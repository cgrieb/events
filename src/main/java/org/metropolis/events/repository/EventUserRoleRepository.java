package org.metropolis.events.repository;

import org.metropolis.events.entity.EventUserRole;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Created by cgrieb on 12/3/16.
 */
@Repository
public interface EventUserRoleRepository  extends JpaRepository<EventUserRole,String> {
    @Override
    @Query("select r from EventUserRole r where r.username =:username")
    @Cacheable(value = "roleCache", key = "#username")
    EventUserRole findOne(@Param(value = "username") String username);
}
