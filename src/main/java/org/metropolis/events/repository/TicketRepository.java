package org.metropolis.events.repository;

import org.metropolis.events.entity.Ticket;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by cgrieb on 12/13/16.
 */
@Repository
public interface TicketRepository extends JpaRepository<Ticket,Integer> {
    @Cacheable(value = "ticketCache", key = "#ticketOwner + '.' + #eventId")
    @Query("select t from Ticket t where t.ticketOwner =:ticketOwner and t.eventId =:eventId")
    Ticket getTicketsFromActiveEvents(@Param(value = "ticketOwner") String ticketOwner, @Param(value = "eventId") int eventId);
}
