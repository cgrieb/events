package org.metropolis.events.service;

import org.metropolis.events.entity.EventUser;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by cgrieb on 11/30/16.
 */
public interface AuthService {
    /**
     * Logs a user into the system.
     * @param request - HttpServletRequest.
     * @return - successfully logged in/did not successfully login.
     */
    boolean login(HttpServletRequest request);

    /**
     * Logs a user out of the system.
     * @return - successfully logged out/did not successfully logout.
     */
    boolean logout();

    /**
     * Is the user authenticated?
     * @return - authenticated/not authenticated.
     */
    boolean getAuthStatus();

    /**
     * Creates a new account.
     * @param eventUser - EventUser object.
     * @return - account created/not created.
     */
    boolean createAccount(EventUser eventUser);

}
