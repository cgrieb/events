package org.metropolis.events.service;

import org.metropolis.events.entity.EventUser;
import org.metropolis.events.entity.EventUserRole;
import org.metropolis.events.repository.EventUserRepository;
import org.metropolis.events.repository.EventUserRoleRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Collection;
import java.util.logging.Logger;

/**
 * Created by cgrieb on 11/30/16.
 */
@Service
class AuthServiceImpl implements AuthService {
    @Resource
    private EventUserRepository eventUserRepository;

    @Resource
    private EventUserRoleRepository eventUserRoleRepository;

    @Resource
    private AuthenticationManager authenticationManager;

    @Resource
    private PasswordEncoder passwordEncoder;

    private static final Logger LOGGER = Logger.getLogger(AuthService.class.getName());

    public boolean createAccount(EventUser eventUser) {
        EventUser doesExist = eventUserRepository.findOne(eventUser.getUsername());
        if(doesExist==null) {
            eventUser.setUsername(eventUser.getUsername().toLowerCase());
            eventUser.setPassword(passwordEncoder.encodeUserPassword(eventUser.getPassword()));

            if(!eventUser.getRole().equals("user")) {
                eventUser.setRole("user");
            }

            EventUserRole eventUserRole = new EventUserRole();
            eventUserRole.setUsername(eventUser.getUsername());
            eventUserRole.setRole(eventUser.getRole());

            eventUserRepository.save(eventUser);
            eventUserRoleRepository.save(eventUserRole);
            return true;
        }

        return false;
    }

    @Override
    public boolean login(HttpServletRequest httpServletRequest) {
        String[] credentials = this.getUsernameAndPassword(httpServletRequest);
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(credentials[0].toLowerCase(),credentials[1]);
        try {
            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return true;
        } catch (final Throwable throwable) {
            LOGGER.warning("** Unable to authenticate user: " + credentials[0] + " please try again **");
        }

        return false;
    }

    @Override
    public boolean logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return SecurityContextHolder.getContext().getAuthentication() == null;
    }

    @Override
    public boolean getAuthStatus() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Collection<? extends GrantedAuthority> authenticationAuthorities = authentication.getAuthorities();
        return  !(authentication.getName().equals("anonymousUser")&&authenticationAuthorities.contains(new SimpleGrantedAuthority("ROLE_ANONYMOUS")));
    }

    private String[] getUsernameAndPassword(HttpServletRequest httpServletRequest) {
        String encryptedCredentials = httpServletRequest.getHeader("Authorization").substring(("Basic").length()).trim();
        return  new String(Base64.getDecoder().decode(encryptedCredentials), Charset.forName("UTF-8")).split(":");
    }
}