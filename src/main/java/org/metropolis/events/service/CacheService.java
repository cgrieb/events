package org.metropolis.events.service;

import java.util.Date;

/**
 * Created by metropolis on 1/3/2017.
 */
public interface CacheService {
    /**
     * Clear *all* system cache.
     * @return - Date in string format.
     */
    Date clearAllSystemCache();
}
