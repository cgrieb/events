package org.metropolis.events.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by metropolis on 1/3/2017.
 */
@Service
public class CacheServiceImpl implements CacheService {
    private static final String TICKET_CACHE = "ticketCache";
    private static final String EVENT_CACHE = "eventCache";
    private static final String USER_CACHE = "userCache";
    private static final String ROLE_CACHE = "roleCache";

    @Override
    @CacheEvict(value = {TICKET_CACHE,EVENT_CACHE,USER_CACHE,ROLE_CACHE}, allEntries = true)
    public Date clearAllSystemCache() {
        return  new Date();
    }
}
