package org.metropolis.events.service;

import org.metropolis.events.entity.Event;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.Map;

/**
 * Created by cgrieb on 12/11/16.
 */
public interface EventService {
    /**
     * Add a new event.
     * @param event - Event object we're adding.
     */
    void addEvent(Event event);

    /**
     * Get all active events.
     * @return - A List of all active events.
     */
    List<Event> getActiveEvents();

    /**
     * Obtains an object with active events and user purchased tickets.
     * @param userDetails - UserDetails object.
     * @return Map String,Object of events and tickets.
     */
    Map<String,Object> getEventsAndTickets(UserDetails userDetails);

    /**
     * Extract a single event based on the eventId.
     * @param eventId - eventId of the event we're looking for.
     * @return Event object.
     */
    Event getEvent(int eventId);
}
