package org.metropolis.events.service;

import org.metropolis.events.entity.Event;
import org.metropolis.events.entity.Ticket;
import org.metropolis.events.repository.EventRepository;
import org.metropolis.events.repository.TicketRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by cgrieb on 12/11/16.
 */
@Service
class EventServiceImpl implements EventService {
    @Resource
    private EventRepository eventRepository;

    @Resource
    private TicketRepository ticketRepository;

    @Override
    public void addEvent(Event event) {
        eventRepository.save(event);
    }

    @Override
    public List<Event> getActiveEvents() {
        return eventRepository.getAllActiveEvents();
    }

    @Override
    public Map<String,Object> getEventsAndTickets(UserDetails userDetails) {
        Map<String,Object> eventsAndTickets = new HashMap<>();

        List<Event> events = eventRepository.getAllActiveEvents();
        List<Ticket> tickets = new ArrayList<>();

        events.forEach(event->{
            Ticket ticket = ticketRepository.getTicketsFromActiveEvents(userDetails.getUsername(),event.getEventId());
            if(ticket!=null) {
                tickets.add(ticket);
            }
        });

        eventsAndTickets.put("events", events);
        eventsAndTickets.put("tickets", tickets);
        return eventsAndTickets;
    }

    @Override
    public Event getEvent(int eventId) {
        return eventRepository.findOne(eventId);
    }
}
