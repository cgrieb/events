package org.metropolis.events.service;

/**
 * Created by cgrieb on 12/3/16.
 */
public interface PasswordEncoder {
    String encodeUserPassword(String password);
}
