package org.metropolis.events.service;

import org.metropolis.events.entity.Ticket;

/**
 * Created by metropolis on 12/16/2016.
 */
public interface TicketService {
    /**
     * Adds/Saves a ticket.
     * @param ticket - Ticket object we're updating.
     * @param username - username of the ticket owner.
     * @return true/false, added/not added.
     */
    boolean purchaseTicketsForEvent(Ticket ticket, String username);
}
