package org.metropolis.events.service;

import org.metropolis.events.entity.Event;
import org.metropolis.events.entity.Ticket;
import org.metropolis.events.repository.EventRepository;
import org.metropolis.events.repository.TicketRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.logging.Logger;

/**
 * Created by metropolis on 12/16/2016.
 */
@Service
public class TicketServiceImpl implements TicketService {
    private static final Logger LOGGER = Logger.getLogger(TicketService.class.getName());

    @Resource
    private TicketRepository ticketRepository;

    @Resource
    private EventRepository eventRepository;

    @Override
    @Caching(evict = {@CacheEvict(value = "ticketCache", key = "#ticket.ticketId + '.' + #username"),
                      @CacheEvict(value = "eventCache", allEntries = true)
    })
    public boolean purchaseTicketsForEvent(Ticket ticket, String username) {
        Event event = eventRepository.findOne(ticket.getEventId());

        int availableTickets = event.getAvailableTickets();
        int purchasedTickets = ticket.getPurchasedTickets();

        if(availableTickets - purchasedTickets >= 0 && event.getTicketPurchaseMax()>=ticket.getPurchasedTickets()) {
            event.setAvailableTickets(availableTickets - purchasedTickets);
            eventRepository.save(event);
            ticketRepository.save(ticket);
            LOGGER.info("** Event  " + event.getTitle() + " has been updated **");
            return true;
        } else {
            LOGGER.warning("** Unable to purchase tickets - requested purchase exceeds tickets available **");
        }

        return false;
    }
}
