angular.module("EventsApp", ["ngRoute", "ngResource", "ngMessages", "ui.bootstrap"])
    .config(function($routeProvider,$locationProvider){
        $routeProvider.when("/login", {
            controller: "LoginController",
            templateUrl: "/static/angular/public/view/login.html",
            requireLogin: false
        }).when("/create", {
            controller: "CreateController",
            templateUrl: "/static/angular/public/view/create.html",
            requireLogin: false
        }).when('/events', {
            controller: "EventController",
            templateUrl: "/static/angular/public/view/events.html",
            requireLogin:true
        }).when("/operations", {
            controller: "OperationController",
            templateUrl: "/static/angular/public/view/operations.html",
            requireLogin:true
        }).when("/edit", {
            controller: "EditController",
            templateUrl: "/static/angular/public/view/edit.html",
            requireLogin:true
        }).when("/info", {
            controller:"InfoController",
            templateUrl:"/static/angular/public/view/info.html",
            requiredLogin:true
        }).when("/purchase", {
            controller:"PurchaseController",
            templateUrl:"/static/angular/public/view/purchase.html",
            requiredLogin:true
        }).when("/about", {
            templateUrl:"/static/angular/public/view/about.html"
        }).when("/cache", {
            templateUrl:"/static/angular/public/view/cache.html",
            controller: "CacheController",
            requiredLogin:true
        }).when("/logout", {
            controller:"LogoutController",
            templateUrl:"/static/angular/public/view/logout.html"
        }).otherwise( {
            redirectTo: "/events"
        });
        $locationProvider.html5Mode(true);
});
