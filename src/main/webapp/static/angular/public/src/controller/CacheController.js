/**
 * Created by metropolis on 1/6/2017.
 */
angular.module('EventsApp').controller('CacheController',['$scope','CacheService',function($scope,CacheService){
    $scope.timeStamp = {};

    initializeCacheController();

    function initializeCacheController() {
        CacheService.clearCache().then(function(response){
            $scope.timeStamp = new Date(response);
        });
    }
}]);