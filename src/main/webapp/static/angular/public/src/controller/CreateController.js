angular.module('EventsApp').controller('CreateController',['$scope','AuthService','$location',function($scope,AuthService,$location){
    $scope.eventUser = {};

    resetEventUser();

    $scope.states = ["AK","AL","AR","AZ","CA","CO","CT","DC","DE","FL","GA","GU","HI","IA","ID", "IL","IN","KS","KY","LA","MA","MD","ME",
        "MH","MI","MN","MO","MS","MT","NC","ND","NE","NH","NJ","NM","NV","NY", "OH","OK","OR","PA","PR","PW","RI","SC","SD","TN","TX","UT","VA","VI","VT","WA","WI","WV","WY"];

    $scope.createUserAccount = function() {
        AuthService.createUserAccount($scope.eventUser).then(function(response) {
            if(response) {
                console.log("** account created successfully for user " + $scope.eventUser.username + " **");
                $location.url('/login');
            } else {
                console.log("** there was an error creating the account for user "  + $scope.eventUser.username + " **");
                resetEventUser();
            }
        })
    };

    function resetEventUser() {
        $scope.eventUser = {
            username:"",
            firstName:"",
            lastName:"",
            address:"",
            city:"",
            state:"AK",
            password:"",
            role:"user",
            enabled:true
        };
    }

    $scope.resetFromView = function() {
        resetEventUser();
    }
}]);