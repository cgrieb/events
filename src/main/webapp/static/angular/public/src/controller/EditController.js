angular.module('EventsApp').controller('EditController',['$scope','$location','EditService','AuthService',function($scope,$location,EditService,AuthService){
    var userDetails = {};
    $scope.event = {};
    $scope.hasEvent = false;

    initializeEditController();

    function initializeEditController() {
        userDetails = JSON.parse(AuthService.getUserDetailsSession());
        $scope.hasEvent = EditService.getHasEvent();
        if($scope.hasEvent) {
            $scope.event = EditService.getEvent();
        } else {
            buildEventObject();
        }
    }

    function buildEventObject() {
        $scope.event = {
            availableTickets:1,
            description:"",
            title:"",
            startDate:new Date(),
            endDate:new Date(),
            enabled:true,
            ticketPrice:18,
            ticketPurchaseMax:10,
            createdBy:userDetails.username
        };
    }

    $scope.buildNewEventObject = function() {
        buildEventObject();
    };

    $scope.submitNewEvent = function() {
        EditService.addEvent($scope.event).then(function(response){
            if(response) {
                console.log("** Event successfully updated **");
                $location.url("/events");
            } else {
                console.log("** There was an error updating the event **");
            }
       });
    };

    $scope.firstDatePicker = {
        opened:false
    };

    $scope.secondDatePicker = {
        opened:false
    };

    $scope.dateOptions = {
        customClass: getDayClass,
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1,
        clearText: "Reset"
    };

    $scope.toggleMin = function() {
        $scope.dateOptions.minDate = $scope.dateOptions.minDate ? null : new Date();
    };

    $scope.toggleMin();

    $scope.setDate = function(year, month, day) {
        $scope.event.startDate = new Date(year, month, day);
        $scope.event.endDate = new Date(year, month, day);
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    var afterTomorrow = new Date(tomorrow);
    afterTomorrow.setDate(tomorrow.getDate() + 1);

    $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
    ];

    function getDayClass(data) {
        var date = data.date,
        mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }

    $scope.openFirstPicker = function() {
        $scope.firstDatePicker.opened = true;
    };

    $scope.openSecondPicker = function() {
        $scope.secondDatePicker.opened = true;
    };

    $scope.determineDateValues = function() {
       if($scope.event.startDateTime == null) {
           $scope.event.startDateTime = new Date();
       }

       if($scope.event.endDateTime == null) {
           $scope.event.endDateTime = new Date();
       }

        compareDates();
    };

    function compareDates() {
       if($scope.event.endDate < $scope.event.startDate) {
           $scope.event.endDate = $scope.event.startDate;
       }
    }
}]);