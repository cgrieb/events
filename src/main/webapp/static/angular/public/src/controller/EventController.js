angular.module('EventsApp').controller('EventController',['$scope','AuthService','EventService','PurchaseInfoService','EditService','$location',function($scope,AuthService,EventService,PurchaseInfoService,EditService,$location){
    $scope.userDetails = {};
    $scope.ticketsAndEvents = {};

    $scope.events = [];
    $scope.tickets = [];

    $scope.eventsAndTicketsLoaded = false;

    initializeEventController();

    function initializeEventController() {
        $scope.userDetails = JSON.parse(AuthService.getUserDetailsSession());
        EventService.getEventsAndTickets().then(function(response){
            $scope.ticketsAndEvents = response;
            $scope.eventsAndTicketsLoaded = true;
        });
    }

    $scope.setEventAndNavigate = function(url,event) {
        var eventAndTicket = {};
        var ticket = {
            ticketId:0,
            ticketOwner:"unassigned",
            purchasedTickets:0,
            purchasedDate:new Date(),
            paidDate:new Date,
            eventId:1,
            paid:false
        };

        eventAndTicket.event = event;
        eventAndTicket.ticket = ticket;

        for (var i = 0, j = $scope.ticketsAndEvents.tickets.length; i < j; i++) {
            if($scope.ticketsAndEvents.tickets[i].eventId === event.eventId) {
                eventAndTicket.ticket = $scope.ticketsAndEvents.tickets[i];
            }
        }

        PurchaseInfoService.setEventAndTicket(eventAndTicket);
        $location.path(url);
    };

    $scope.setEventAndEdit = function(event) {
        EditService.setEvent(event);
        $location.url('/edit');
    };

    $scope.determineUserCanPurchase = function(event) {
        if(event.availableTickets === 0) {
            return false;
        }

        var ret = true;

        for (var i = 0, j = $scope.ticketsAndEvents.tickets.length; i < j; i++) {
            var tickets = $scope.ticketsAndEvents.tickets[i];
            if(tickets.eventId == event.eventId) {
                if(tickets.purchasedTickets >= event.ticketPurchaseMax) {
                    ret = false;
                }
            }

        }

        return ret;
    };

    $scope.getPurchasedTickets = function(event) {
        for(var i = 0, j = $scope.ticketsAndEvents.tickets.length; i < j; i++) {
            var ticket = $scope.ticketsAndEvents.tickets[i];
            if(ticket.eventId === event.eventId) {
                return ticket.purchasedTickets;
            }
        }
        return 0;
    }
}]);