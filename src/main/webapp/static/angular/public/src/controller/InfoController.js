angular.module('EventsApp').controller('InfoController',['$scope','AuthService','PurchaseInfoService','$location',function($scope,AuthService,PurchaseInfoService,$location){
    $scope.event = {};
    $scope.ticket = {};

    initializeInfoController();

    function initializeInfoController() {
        var eventAndTicket = PurchaseInfoService.getEventAndTicket();
        var objectLength = getObjectLength(eventAndTicket);

        if(objectLength>0) {
            $scope.event = eventAndTicket.event;
            $scope.ticket = eventAndTicket.ticket;
        } else {
            $location.url('/events');
        }
    }

    function getObjectLength(object) {
        return Object.keys(object).length
    }
}]);