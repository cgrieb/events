angular.module('EventsApp').controller('LoginController',['$scope','AuthService','$window',function($scope,AuthService,$window){
    $scope.password = "";
    $scope.username = "";
    $scope.displayFailedLogin = false;

    $scope.login = function() {
        AuthService.login($scope.username,$scope.password).then(function(response){
            if(response) {
                AuthService.getUserDetails().then(function(response){
                    if(response) {
                        if($scope.displayFailedLogin) {
                            $scope.displayFailedLogin = true;
                        }
                        $scope.userDetails = AuthService.getUserDetails();
                        $window.location = "/events";
                    }
                });
            } else {
                resetCredentials();
            }
        });
    };

    function resetCredentials() {
        $scope.username = "";
        $scope.password = "";
        $scope.displayFailedLogin = true;
    }
}]);