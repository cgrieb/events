/**
 * Created by metropolis on 1/2/2017.
 */
angular.module('EventsApp').controller('LogoutController',['$scope','AuthService','PurchaseInfoService','$location',function($scope,AuthService,PurchaseInfoService,$location){
    AuthService.logout().then(function(response){
        if(response) {
            console.log("Logout successful");
            $location.url('/login');
        } else {
            console.log("There was an error logging out");
        }
    });
}]);