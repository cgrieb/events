/**
 * Created by metropolis on 12/17/2016.
 */
angular.module('EventsApp').controller('PurchaseController', ['$scope','PurchaseInfoService','AuthService','PurchaseService','$location',function($scope,PurchaseInfoService,AuthService,PurchaseService,$location){
    $scope.ticketsAndEvent = {};
    $scope.event = {};
    $scope.ticket = {};
    $scope.userDetails = {};

    $scope.availableTicketsForPurchase = [];
    $scope.months = [];
    $scope.years = [];

    $scope.ticketsForPurchase = "1";

    $scope.creditCardData = {
        cardOwner:"",
        cardNumber:"",
        cardCVSNumber:"",
        cardExpireYear:new Date().getFullYear(),
        cardExpireMonth:"01"
    };

    $scope.ticketSaved = false;
    $scope.purchaseComplete = false;

    initializePurchaseController();

    function initializePurchaseController() {
        var eventAndTicket = PurchaseInfoService.getEventAndTicket();
        if(getObjectLength(eventAndTicket)>0) {
            $scope.event = eventAndTicket.event;
            $scope.ticket = eventAndTicket.ticket;

            var year = new Date().getFullYear();
            for (var x = 0; x < 10;x++) {
                $scope.years.push(year.toString());
                year = year + 1;
            }

            for (var y = 1; y < 13; y++ ) {
                if(y < 10) {
                    $scope.months.push("0" + y.toString())
                } else {
                    $scope.months.push(y.toString())
                }
            }

            $scope.userDetails = JSON.parse(AuthService.getUserDetailsSession());
            updateAvailableTickets();
        } else {
            $location.url('/events');
        }
    }

    $scope.validatePurchaseInformation = function() {
        if($scope.creditCardData.cardNumber.length>0) {
            var creditCardNumber = $scope.creditCardData.cardNumber.replace(/ /g, "");
        } else {
            creditCardNumber = "";
        }

        /**
         * We only care about number for this since we're not specifying
         * a credit card type.
         */
        return $scope.creditCardData.cardOwner.length > 2 && $scope.creditCardData.cardCVSNumber.length === 3
            && creditCardNumber.search(/^\d+$/) >= 0 && $scope.userDetails.username.length > 2;
    };

    $scope.completePurchase = function() {
        var date = new Date();
        var tempTicket = {};

        angular.copy($scope.ticket,tempTicket);

        tempTicket.purchasedDate = date;
        tempTicket.paidDate = date;

        tempTicket.eventId = $scope.event.eventId;
        tempTicket.ticketOwner = $scope.userDetails.username;

        tempTicket.purchasedTickets += parseInt($scope.ticketsForPurchase);
        tempTicket.paid = true;

        /**
         * Technically, we should hit the rest service for this - but it saves
         * a call to the database and we're assuming that the purchase was successful.
         */
        PurchaseService.purchaseTickets(tempTicket).then(function(response){
            $scope.ticketSaved = response;
            $scope.purchaseComplete = true;

            if(response) {
                $scope.creditCardData = {};
                angular.copy(tempTicket,$scope.ticket);
                $scope.updateAvailableTickets();
            }
        });
    };

    $scope.updateAvailableTickets = function() {
        updateAvailableTickets();
    };

    function updateAvailableTickets() {
        var ticketsAvailableForPurchase = $scope.event.ticketPurchaseMax - $scope.ticket.purchasedTickets;

        if($scope.availableTicketsForPurchase.length>0) {
            $scope.availableTicketsForPurchase = [];
        }

        for(var i = 1, j = ticketsAvailableForPurchase + 1; i < j; i++) {
            $scope.availableTicketsForPurchase.push(i.toString());
        }
    }

    function getObjectLength(object) {
        return Object.keys(object).length;
    }
}]);