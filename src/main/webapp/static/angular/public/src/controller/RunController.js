angular.module('EventsApp').controller('RunController', ['$scope','$location','$interval','$document','$window','AuthService', function($scope,$location,$interval,$document,$window,AuthService){
    $scope.userDetails = {};
    $scope.authenticated = false;
    $scope.isAdmin = false;

    var ANONYMOUS_USER = "anonymousUser";
    var maxInterval = 180;
    var interval = 10;

    initializeRunController();

    function initializeRunController() {
        AuthService.getAuthStatus().then(function(response){
            $scope.authenticated = response;
            if(response) {
                $scope.userDetails = JSON.parse(AuthService.getUserDetailsSession());
                var authorities = $scope.userDetails.authorities;
                for (var i = 0, j = authorities.length; i < j; i++) {
                    if(authorities[i].authority == "admin" && !$scope.isAdmin) {
                        $scope.isAdmin = true;
                    }
                }

                if($scope.userDetails.authorities)
                var url = $location.path();
                $location.path(url);
            } else {
                AuthService.clearSessionStorage();
                $location.path("/login");
            }
        });
    }
}]);