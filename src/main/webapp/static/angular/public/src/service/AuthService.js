angular.module('EventsApp').service('AuthService', ['$http','$q',function($http,$q){
    var TIMEOUT = 10000;
    var userDetails = {};
    var userDetailsKey = "userDetails";

    this.getAuthStatus = function() {
        var deferred = $q.defer();
        $http({
            url:'/auth/getAuthStatus',
            method:'GET',
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });

        return deferred.promise;
    };

    this.login = function(username,password) {
        var deferred = $q.defer();
        var authHeader = createAuthHeader(username,password);
        $http({
            url:'/auth/login',
            method:'GET',
            timeout:TIMEOUT,
            headers:{'Authorization':authHeader}
        }).success(function(response){
            deferred.resolve(response);
        });

        return deferred.promise;
    };

    this.logout = function() {
        var deferred = $q.defer();
        $http({
            url:'/auth/logout',
            method:'GET',
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });

        return deferred.promise;
    };

    this.createUserAccount = function(eventUser) {
        var deferred = $q.defer();
        $http({
           url:'/auth/createUserAccount',
            method:'POST',
            timeout:TIMEOUT,
            data:JSON.stringify(eventUser)
        }).success(function(response){
            deferred.resolve(response);
        });

        return deferred.promise;
    };

    this.getUserDetails = function() {
        var deferred = $q.defer();
        $http({
            url:'/auth/getUserDetails',
            method:'POST',
            timeout:TIMEOUT
        }).success(function(response){
            if(response!==null) {
                setUserDetailsSession(response);
                deferred.resolve(true);
            } else {
                deferred.resolve(false);
            }
        });

        return deferred.promise;
    };

    this.getUserDetailsSession = function() {
        return sessionStorage.getItem(userDetailsKey);
    };

    this.clearSessionStorage = function() {
        sessionStorage.clear();
    };

    function createAuthHeader(username, password) {
        return 'Basic ' + window.btoa(username + ':' + password);
    }

    function setUserDetailsSession(userDetails) {
        sessionStorage.clear();
        sessionStorage.setItem(userDetailsKey, JSON.stringify(userDetails));
    }
}]);