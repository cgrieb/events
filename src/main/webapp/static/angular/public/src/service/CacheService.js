/**
 * Created by metropolis on 1/6/2017.
 */
angular.module('EventsApp').service('CacheService', ['$http','$q',function($http,$q){
    var TIMEOUT = 10000;
    this.clearCache = function() {
        var deferred = $q.defer();
        $http({
            url:'/admin/clearAllSystemCache',
            method:'GET',
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
            console.log("response = " + response);
        });

        return deferred.promise;
    };
}]);