angular.module('EventsApp').service('EditService', ['$http','$q',function($http,$q){
    var TIMEOUT = 10000;
    var event = {};
    var hasEvent = false;

    this.addEvent = function(event) {
        var deferred = $q.defer();
        $http({
            url:'/event/addEvent',
            method:'POST',
            timeout:TIMEOUT,
            data:JSON.stringify(event)
        }).success(function(){
            deferred.resolve(true);
        }).error(function(){
            deferred.resolve(false)
        });

        return deferred.promise;
    };

    this.setEvent = function(eventToStore) {
        event = eventToStore;
        hasEvent = true;
    };

    this.getEvent = function() {
        hasEvent = false;
        return event;
    };

    this.getHasEvent = function() {
        return hasEvent;
    }
}]);