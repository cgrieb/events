angular.module('EventsApp').service('EventService', ['$http','$q',function($http,$q){
    var TIMEOUT = 10000;

    this.getEventsAndTickets = function() {
        var deferred = $q.defer();
        $http({
            url:'/event/getEventsAndTickets',
            method:'GET',
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });

        return deferred.promise;
    };

    this.getActiveEvents = function() {
        var deferred = $q.defer();
        $http({
            url:'/event/getActiveEvents',
            method:'GET',
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });

        return deferred.promise;
    };

    this.getTickets = function() {
        var deferred = $q.defer();
        $http({
            url:'/event/getTickets',
            method:'GET',
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });

        return deferred.promise;
    }
}]);