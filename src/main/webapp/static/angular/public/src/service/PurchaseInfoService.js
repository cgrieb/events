angular.module('EventsApp').service('PurchaseInfoService', [function(){
    this.eventAndTicket = {};
    this.setEventAndTicket = function(eventAndTicket) {
        this.eventAndTicket = eventAndTicket;
    };

    this.getEventAndTicket = function() {
        return this.eventAndTicket;
    }
}]);