/**
 * Created by metropolis on 12/27/2016.
 */
angular.module('EventsApp').service('PurchaseService', ['$q','$http',function($q,$http){
    var TIMEOUT = 10000;

    this.purchaseTickets = function(tickets) {
        var deferred = $q.defer();
        $http({
            url:'/event/purchaseTicketsForEvent',
            method:'POST',
            data:JSON.stringify(tickets),
            timeout:TIMEOUT
        }).success(function(response){
            deferred.resolve(response);
        });
        return deferred.promise;
    }
}]);