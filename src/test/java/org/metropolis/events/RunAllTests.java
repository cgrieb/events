package org.metropolis.events;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.metropolis.events.configuration.EventCacheConfigurationTest;
import org.metropolis.events.configuration.EventDataConfigurationTest;
import org.metropolis.events.controller.*;
import org.metropolis.events.service.*;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by metropolis on 1/13/2017.
 */
@ContextConfiguration(locations = {"file:src/test/webapp/WEB-INF/test-config.xml"})
@RunWith(Suite.class)
@Suite.SuiteClasses({
        PasswordEncoderTest.class,
        AuthServiceTest.class,
        TicketServiceTest.class,
        EventServiceTest.class,
        EventDataConfigurationTest.class,
        EventCacheConfigurationTest.class,
        MainControllerTest.class,
        CacheControllerTest.class,
        EventControllerTest.class,
        AuthControllerTest.class})
public class RunAllTests   {
}
