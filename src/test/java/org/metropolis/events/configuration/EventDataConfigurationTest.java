package org.metropolis.events.configuration;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.metropolis.events.EventsContextBootStrap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.logging.Logger;

@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
@ContextConfiguration(classes = EventDataConfiguration.class)
public class EventDataConfigurationTest {
    @Autowired
    DataSource dataSource;

    private static final Logger LOGGER = Logger.getLogger(EventDataConfigurationTest.class.getName());

    //*******************************************************************
    // Note: you'll need to update these finals to your specific database
    // information other wise it'll fail.
    //*******************************************************************
    private static final String URL = "jdbc:mysql://localhost:3306/metropolis";
    private static final String USERNAME = "root@localhost";

    @Test
    public void verifyDatabaseConnection() {
       try {
           Connection connection = dataSource.getConnection();
           DatabaseMetaData databaseMetaData = connection.getMetaData();
           Assert.assertEquals(databaseMetaData.getUserName(),USERNAME);
           Assert.assertEquals(databaseMetaData.getURL(), URL);
       } catch (SQLException e) {
            LOGGER.warning(e.getMessage());
       }
    }
}
