package org.metropolis.events.controller;

import org.junit.Before;
import org.junit.Test;
import org.metropolis.events.EventsContextBootStrap;
import org.metropolis.events.helper.AuthHeaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;
import java.util.logging.Logger;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

/**
 * Created by metropolis on 1/14/2017.
 */

/**
 * Note: since there isn't any logic in the controllers, we're just confirming that the @RequestMappings are
 * correctly configured.
 */
public class AuthControllerTest extends EventsContextBootStrap {
    @Resource
    private AuthHeaderBuilder authHeaderBuilder;

    private static final Logger LOGGER = Logger.getLogger(AuthControllerTest.class.getName());

    private MockMvc mockMvc;
    private static final String USERNAME = "role.admin@email.com";
    private static final String PASSWORD = "password";
    private static final String ROLE = "admin";

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void authenticateUser() {
        String credentials = authHeaderBuilder.generateAuthHeader(USERNAME, PASSWORD);
        try {
            mockMvc.perform(get("/auth/login").header("Authorization", credentials)).andExpect(status().isOk());
        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
        }
    }

    @Test
    public void getUserDetails() {
        try {
            mockMvc.perform(post("/auth/getUserDetails").with(user(USERNAME).roles(ROLE).password(PASSWORD)))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
        }
    }

    @Test
    public void getAuthStatus() {
        try {
            mockMvc.perform(get("/auth/getAuthStatus").with(user(USERNAME).roles(ROLE)
                    .password(PASSWORD))).andExpect(status().isOk());
        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
        }
    }

    @Test
    public void logout() {
        try {
            mockMvc.perform(get("/auth/logout").with(user(USERNAME).roles(ROLE)
                    .password(PASSWORD))).andExpect(status().isOk());
        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
        }
    }
}
