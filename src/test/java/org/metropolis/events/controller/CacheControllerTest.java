package org.metropolis.events.controller;

import org.junit.Before;
import org.junit.Test;
import org.metropolis.events.EventsContextBootStrap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.logging.Logger;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by metropolis on 1/15/2017.
 */
public class CacheControllerTest extends EventsContextBootStrap {
    private static final Logger LOGGER = Logger.getLogger(AuthControllerTest.class.getName());

    @Autowired
    protected WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;
    private static final String USERNAME = "role.admin@email.com";
    private static final String PASSWORD = "password";
    private static final String ROLE = "admin";

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void getActiveEvents() {
        try {
            mockMvc.perform(get("/admin/clearAllSystemCache").with(user(USERNAME).roles(ROLE)
                   .password(PASSWORD))).andExpect(status().isOk());
        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
        }
    }
}
