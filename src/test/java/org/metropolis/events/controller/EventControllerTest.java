package org.metropolis.events.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Before;
import org.junit.Test;
import org.metropolis.events.EventsContextBootStrap;
import org.metropolis.events.entity.Event;
import org.metropolis.events.entity.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.*;
import java.util.logging.Logger;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

/**
 * Created by metropolis on 1/14/2017.
 */

/**
 * Note: since there isn't any logic in the controllers, we're just confirming that the @RequestMappings are
 * correctly configured.
 */
public class EventControllerTest extends EventsContextBootStrap {
    private static final Logger LOGGER = Logger.getLogger(EventController.class.getName());

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private static final String ADMIN_USER = "role.admin@email.com";
    private static final String USER_USER  = "role.user@email.com";
    private static final String PASSWORD   = "password";
    private static final String ROLE_USER  = "user";
    private static final String ROLE_ADMIN = "admin";

    private MockMvc mockMvc;
    private static ObjectMapper objectMapper;

    private Event event;
    private Ticket ticket;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);

        this.mockMvc = webAppContextSetup(this.webApplicationContext).build();

        event = new Event();
        event.setEventId(3);
        event.setAvailableTickets(1000);
        event.setCreatedBy(ADMIN_USER);
        event.setTitle("REST Test");
        event.setDescription("Testing add event REST service");
        event.setEnabled(false);
        event.setTicketPrice(10);
        event.setStartDate(new Date());
        event.setEndDate(new Date());
        event.setTicketPurchaseMax(5);

        ticket = new Ticket();
        ticket.setTicketId(1);
        ticket.setPaid(true);
        ticket.setPaidDate(new Date());
        ticket.setPurchasedDate(new Date());
        ticket.setPurchasedTickets(10);
        ticket.setTicketOwner(USER_USER);
        ticket.setEventId(1);
    }

    @Test
    public void addEvent() {
        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        try {
            String request = objectWriter.writeValueAsString(event);
            try {
                mockMvc.perform(post("/event/addEvent").contentType(APPLICATION_JSON_UTF8).content(request).with(user(ADMIN_USER)
                       .roles(ROLE_ADMIN).password(PASSWORD))).andExpect(status().isOk());
            } catch (Exception e) {
                LOGGER.warning(e.getMessage());
            }
        } catch (JsonProcessingException e) {
            LOGGER.warning(e.getMessage());
        }
    }

    @Test
    public void getActiveEvents() {
        try {
            mockMvc.perform(get("/event/getActiveEvents").with(user(ADMIN_USER).roles(ROLE_ADMIN)
                   .password(PASSWORD))).andExpect(status().isOk());
        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
        }
    }

    @Test
    public void getEventsAndTickets() {
        try {
            mockMvc.perform(get("/event/getEventsAndTickets").with(user(ADMIN_USER).roles(ROLE_ADMIN)
                   .password(PASSWORD))).andExpect(status().isOk());
        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
        }
    }

    @Test
    public void purchaseTickets() {
        ObjectWriter objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
        try {
            String request = objectWriter.writeValueAsString(ticket);
            try {
                mockMvc.perform(post("/event/purchaseTicketsForEvent").contentType(APPLICATION_JSON_UTF8).content(request)
                       .with(user(USER_USER).roles(ROLE_USER).password(PASSWORD))).andExpect(status().isOk());
            } catch (Exception e) {
                LOGGER.warning(e.getMessage());
            }
        } catch (JsonProcessingException e) {
            LOGGER.warning(e.getMessage());
        }
    }
}
