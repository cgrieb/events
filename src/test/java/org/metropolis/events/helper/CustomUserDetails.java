package org.metropolis.events.helper;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * Created by metropolis on 1/10/2017.
 */
public class CustomUserDetails implements UserDetails {
    private final String name;
    private final String username;
    private final String password;
    private final Collection<? extends GrantedAuthority> authorities;

    public CustomUserDetails() {
        this.authorities = AuthorityUtils.createAuthorityList("user", "admin");
        this.name = "";
        this.username = "";
        this.password = "";
    }

    public CustomUserDetails(String name, String username) {
        this.name = name;
        this.username = username;
        this.authorities = AuthorityUtils.createAuthorityList("user", "admin");
        this.password = "";
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String toString() {
        return "CustomUserDetails{" +
                "username='" + username + '\'' +
                '}';
    }
}
