package org.metropolis.events.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.metropolis.events.EventsContextBootStrap;
import org.metropolis.events.entity.EventUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;


import java.util.Base64;

/**
 * Created by metropolis on 1/12/2017.
 */
public class AuthServiceTest extends EventsContextBootStrap {
    @Autowired
    private AuthService authService;
    private static EventUser eventUser;
    private static final String username = "role.user@email.com";
    private static final String password = "password";

    @Before
    public void setup() throws Exception {
        eventUser = new EventUser();
        eventUser.setFirstName("Test");
        eventUser.setLastName("User");
        eventUser.setCity("Some City");
        eventUser.setState("CA");
        eventUser.setUsername(username);
        eventUser.setRole("user");
        eventUser.setAddress("401 West Matthews");
        eventUser.setPassword(password);
    }

    @Test
    public void addNewUser() throws Exception{
        boolean results = authService.createAccount(eventUser);
        Assert.assertTrue(results);
    }

    @Test
    public
    void authenticateNewUser() throws Exception{
        Base64.Encoder encoder = Base64.getEncoder();
        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest();
        String authHeaderString = username + ":" + password;
        mockHttpServletRequest.addHeader("Authorization", "Basic " + encoder.encodeToString(authHeaderString.getBytes()));
        boolean results = authService.login(mockHttpServletRequest);
        Assert.assertTrue(results);
    }
}
