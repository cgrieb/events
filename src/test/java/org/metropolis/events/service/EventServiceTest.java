package org.metropolis.events.service;

import org.junit.Assert;
import org.junit.Test;
import org.metropolis.events.EventsContextBootStrap;
import org.metropolis.events.entity.Event;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.Date;
import java.util.List;

/**
 * Created by metropolis on 1/8/2017.
 */
public class EventServiceTest  extends EventsContextBootStrap  {
    @Autowired
    private EventService eventService;

    @Test
    public void getActiveEvents() {
        List<Event> events = eventService.getActiveEvents();
        Assert.assertEquals(events.size(),2);
    }

    @Test
    public void addNewEvent() {
        Date date = new Date();
        Event event = new Event();
        event.setDescription("This is a new Event");
        event.setTitle("A new Event");
        event.setCreatedBy("role.admin@email.com");
        event.setEnabled(true);
        event.setAvailableTickets(1000);
        event.setTicketPurchaseMax(10);
        event.setTicketPrice(10);
        event.setStartDate(date);
        event.setEndDate(date);

        eventService.addEvent(event);
        List<Event> events = eventService.getActiveEvents();
        Assert.assertEquals(events.size(),3);
    }

    @Test
    public void disableEvent() {
        List<Event> events = eventService.getActiveEvents();
        events.forEach(event -> {
            if(event.getEventId()==3) {
                event.setEnabled(false);
                eventService.addEvent(event);
            }
        });
    }

    @Test
    public void getUpdatedActiveEvents() {
        List<Event> events = eventService.getActiveEvents();
        Assert.assertEquals(events.size(),2);
    }
    
}
