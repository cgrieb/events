package org.metropolis.events.service;

import org.junit.*;
import org.metropolis.events.EventsContextBootStrap;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Created by metropolis on 1/8/2017.
 */
public class PasswordEncoderTest extends EventsContextBootStrap {
    @Autowired
    PasswordEncoder passwordEncoder;

    public PasswordEncoderTest() {

    }

    private static final String password = "password";
    private static final String encoded = "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8";

    @Test
    public void testPasswordEncoding() {
        Assert.assertEquals(passwordEncoder.encodeUserPassword(password),encoded);
    }
}
