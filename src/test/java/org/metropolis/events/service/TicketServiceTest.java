package org.metropolis.events.service;

import org.junit.*;
import org.junit.Before;
import org.junit.Test;
import org.metropolis.events.EventsContextBootStrap;
import org.metropolis.events.entity.Event;
import org.metropolis.events.entity.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;


import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by metropolis on 1/8/2017.
 */
public class TicketServiceTest extends EventsContextBootStrap {
    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private TicketService ticketService;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private EventService eventService;

    @Autowired
    AuthenticationManager authenticationManager;

    private static Ticket firstTicket;
    private static Ticket secondTicket;
    private static Ticket thirdTicket;
    private static UserDetails userDetails;

    @Before
    public void setup() {
        Date date = new Date();
        firstTicket = new Ticket();
        firstTicket.setTicketId(1);
        firstTicket.setPaid(true);
        firstTicket.setTicketOwner("role.user@email.com");
        firstTicket.setPurchasedTickets(10);
        firstTicket.setEventId(1);
        firstTicket.setPurchasedDate(date);
        firstTicket.setPaidDate(date);

        secondTicket = new Ticket();
        secondTicket.setPaid(true);
        secondTicket.setTicketOwner("role.user@email.com");
        secondTicket.setPurchasedTickets(11);
        secondTicket.setEventId(2);
        secondTicket.setPurchasedDate(date);
        secondTicket.setPaidDate(date);

        thirdTicket = new Ticket();
        firstTicket.setTicketId(2);
        thirdTicket.setPaid(true);
        thirdTicket.setTicketOwner("role.user@email.com");
        thirdTicket.setPurchasedTickets(10);
        thirdTicket.setEventId(2);
        thirdTicket.setPurchasedDate(date);
        thirdTicket.setPaidDate(date);

        userDetails = this.setAuthentication();
    }

    @Test
    public void purchaseAFewTickets() {
        boolean purchased = ticketService.purchaseTicketsForEvent(firstTicket, firstTicket.getTicketOwner());
        Event event = eventService.getEvent(firstTicket.getEventId());

        Assert.assertTrue(purchased);
        Assert.assertNotNull(event);
        Assert.assertEquals(event.getAvailableTickets(),99990);
        Assert.assertEquals(event.getTicketPurchaseMax(),firstTicket.getPurchasedTickets());
    }

    @Test
    public void tooManyTickets() {
        boolean purchased = ticketService.purchaseTicketsForEvent(secondTicket, secondTicket.getTicketOwner());
        Event event = eventService.getEvent(2);
        Assert.assertFalse(purchased);
        Assert.assertNotEquals(event.getTicketPurchaseMax(),secondTicket.getPurchasedTickets());
    }

    @Test
    public void purchaseAFewMoreTicket() {
        boolean purchased = ticketService.purchaseTicketsForEvent(thirdTicket, thirdTicket.getTicketOwner());
        Event event = eventService.getEvent(2);
        Assert.assertTrue(purchased);
        Assert.assertEquals(event.getTicketPurchaseMax(), thirdTicket.getPurchasedTickets());
    }

    @Test
    public void getEventsAndTickets() {
        Map<String,Object> ticketsAndEvents = eventService.getEventsAndTickets(userDetails);
        for(Map.Entry<String,Object> entry : ticketsAndEvents.entrySet()) {
            if(entry.getKey().equals("events")) {
                List<Event> events = (List<Event>)entry.getValue();
                events.forEach(event -> {
                    Assert.assertEquals(event.getAvailableTickets(),99990);
                    Assert.assertEquals(event.getCreatedBy(), "role.admin@email.com");
                    Assert.assertEquals(event.getTicketPurchaseMax(),10);
                    Assert.assertEquals(event.getTicketPrice(),18);
                });
            }

            if(entry.getKey().equals("tickets")) {
                List<Ticket> tickets = (List<Ticket>)entry.getValue();
                tickets.forEach(ticket -> {
                    Assert.assertEquals(ticket.getTicketOwner(), "role.user@email.com");
                    Assert.assertEquals(ticket.getPurchasedTickets(), 10);
                    Assert.assertEquals(ticket.isPaid(),true);
                });
            }
        }
    }

    private UserDetails setAuthentication() {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("role.user@email.com", "password");
        Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDetails;
    }


}
